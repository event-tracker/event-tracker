//
//  Events.swift
//  Tracker
//
//  Created by CJ Good on 11/7/20.
//  Copyright © 2020 CJ Good. All rights reserved.
//

import UIKit

struct Events: Codable {
    var type: String
    var symptoms: String
    var time: Date
}
